# adminify-api

[![npm (scoped)](https://img.shields.io/npm/v/adminify-api.svg)](https://www.npmjs.com/package/adminify-api)
[![npm bundle size (minified)](https://img.shields.io/bundlephobia/min/adminify-api.svg)](https://www.npmjs.com/package/adminify-api)

API-drive admin package for Laravel

## Install

```
$ npm install adminify-api
```
